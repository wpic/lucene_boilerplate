package com.wpic.lucene;

import net.htmlparser.jericho.Source;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.cjk.*;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.AttributeFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

/**
 * InputTokenizer
 *
 * v0.1
 *
 * Created by addam on 20/04/16.
 */
public class InputTokenizer {

    private StandardTokenizer tokenizerEN;
    private StandardTokenizer tokenizerZH;
    private Analyzer cjkAnalyzer;
    private AttributeFactory factory;

    InputTokenizer() {
        this.factory     = CJKWidthFilter.DEFAULT_TOKEN_ATTRIBUTE_FACTORY; //AttributeFactory.DEFAULT_ATTRIBUTE_FACTORY;
        this.tokenizerEN = new StandardTokenizer(factory);
        this.tokenizerZH = new StandardTokenizer(factory);
        this.cjkAnalyzer = new CJKAnalyzer();
    }


    public ArrayList<String> tokenizeStringEN(String inputString) throws IOException {

        // Strip HTML with Jericho
        Source source = new Source(inputString);
        inputString = source.getTextExtractor().toString();

        // Prepare tokenizer for reading
        tokenizerEN.setReader(new StringReader(inputString));
        tokenizerEN.reset();

        // Initialize return ArrayList
        ArrayList<String> extractedTokens = new ArrayList();

        CharTermAttribute attr = tokenizerEN.addAttribute(CharTermAttribute.class);
        do {
            // Get the token and append to list
            extractedTokens.add(attr.toString());
        } while (tokenizerEN.incrementToken());

        return extractedTokens;
    }


    public void tokenizeStringZH(String inputString) throws IOException {
        // TODO: Tokenize Chinese input
    }

}
