package com.wpic.lucene;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.QueryBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * LuceneIndex
 *
 * v0.1
 *
 * Created by addam on 21/04/16.
 */
public class LuceneIndex {

    private Directory index;
    private Analyzer analyzer;
    private IndexWriterConfig conf;
    private IndexWriter writer;

    public LuceneIndex() throws IOException {
        this.index      = new RAMDirectory();
        this.analyzer   = new StandardAnalyzer(); //new CJKAnalyzer();
        this.conf       = new IndexWriterConfig(analyzer);
        this.writer     = new IndexWriter(index, conf);
    }

    public void indexDoc(String content) throws IOException {
        Document doc = new Document();

        // Prepare doc for writing
        doc.add(new TextField("content", content, Field.Store.YES));
        // Write doc to dir
        writer.addDocument(doc);
        // Call to avoid IndexNotFound error
        close();
    }

    public void searchIndex(ArrayList<String> tokenizedInput) throws IOException {

        QueryBuilder qb = new QueryBuilder(analyzer);
        Query q;

        List<Explanation> resList = new ArrayList();
        List<KWResult> kwResults = new ArrayList();
        ScoreDoc[] hits = {};

        for(String currentInput: tokenizedInput) {

            if(tokenizedInput.isEmpty()) {

            } else {
                // Build and initialize search query
                q = qb.createPhraseQuery("content", currentInput);

                // Set up search parameters
                int hitsPerPage = 10;
                IndexReader reader = DirectoryReader.open(index);
                IndexSearcher searcher = new IndexSearcher(reader);

                try {

                    // set up query runner
                    TopDocs docs = searcher.search(q, hitsPerPage);
                    hits = docs.scoreDocs;
                    String[] results = {};

                    // check there are docs to run the query on
                    if(hits != null && hits.length > 0) {
                        // run the query
                        kwResults.add(new KWResult(currentInput, searcher.explain(q, hits[0].doc)));
                    }

                } catch(NullPointerException e) {
                    //hits = {};
                }

            }
        }

        // sort results by score (descending)
        Collections.sort(kwResults, new Comparator<KWResult>() {
            public int compare(KWResult kw1, KWResult kw2) {
                return Float.compare(kw2.getE().getValue(), kw1.getE().getValue());
            }
        });

        prettyPrintResultsList(kwResults, hits);

    }


    //private void prettyPrintResultsList(List<Explanation> results, ScoreDoc[] hits) {
    private void prettyPrintResultsList(List<KWResult> results, ScoreDoc[] hits) {
        // Start message
        System.out.println("These were the most used keywords on your page:");

        // Store duplicates
        List<String> duplicates = new ArrayList();

        // Per result values
        for(KWResult res: results) {
            if(duplicates.contains(res.getKw())) {
                // skip if duplicate word
            } else {
                String freq = res.getE().toString().split("\n")[3].trim().split("=")[0];
                System.out.println("\'" + res.getKw() + "\' was found " + freq + "times.");
                duplicates.add(res.getKw());
            }
        }
    }

    public void close() throws IOException {
        // Keep available in case of unforeseen need to close dir
        writer.close();
    }

}
