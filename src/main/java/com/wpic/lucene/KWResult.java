package com.wpic.lucene;

import org.apache.lucene.search.Explanation;

/**
 * Created by addam on 13/05/16.
 */
public class KWResult {

    private String kw;
    private Explanation e;

    public KWResult(String kw, Explanation e) {
        this.kw = kw;
        this.e = e;
    }

    public String getKw() { return kw; }

    public Explanation getE() { return e; }

}
