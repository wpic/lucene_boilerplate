package com.wpic.lucene;

import java.util.ArrayList;

/**
 * Main Class
 *
 * v0.1
 *
 * Created by addam on 20/04/16.
 */
public class Main {

    public static void main(final String[] args) throws Exception {

        // Create tokenizer obj
        InputTokenizer inputTokenizer = new InputTokenizer();
        // Send strings to language tokenizers
        ArrayList<String> tokenizedInput;

        // English tokenizer call
        //String contentString = "<h1>Tokenize Tokenize Tokenize me! 你好 电脑</h1>";
        String contentString =
        "<!DOCTYPE html>" +
        "<html>" +
        "<body> <p>Numbers: Un, deux, trois</p>" +
        "<h1>Numbers: One, two, three</h1>" +
        "<p>Numbers: Yi, er, san</p>" +
        "<h1>Numbers: One, two, three</h1>" +
        "</body>" +
        "</html>";

        tokenizedInput = inputTokenizer.tokenizeStringEN(contentString);

        // Chinese tokenizer call
        //tokenizedInput = InputTokenizer.tokenizeStringZH("Tokenize me! 你好 电脑");
        //System.out.println(tokenizedInput);

        // Add new file to Lucene index
        LuceneIndex i = new LuceneIndex();

        i.indexDoc(contentString);

        i.searchIndex(tokenizedInput);

    }



}
